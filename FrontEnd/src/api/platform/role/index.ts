import request from '/@/utils/request';

export function getList(params: object) {
	return request({
		url: '/platform/role/list',
		method: 'get',
		params,
	});
}
export function add(params: object) {
	return request({
		url: '/platform/role',
		method: 'post',
		data: params
	});
}
export function edit(params: object) {
	return request({
		url: '/platform/role/edit',
		method: 'post',
		data: params
	});
}
export function changeStatus(params: object) {
	return request({
		url: '/platform/role/change-status',
		method: 'post',
		data: params
	});
}
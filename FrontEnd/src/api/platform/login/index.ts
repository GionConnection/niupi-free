import request from '/@/utils/request';

/**
 * 获取用户信息
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function getUserInfo(params: object) {
	return request({
		url: '/platform/user/user-info',
		method: 'get',
		params,
	});
}
/**
 * 用户登录
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function signIn(params: object) {
	return request({
		url: '/platform/user/login',
		method: 'post',
		data: params
	});
}
/**
 * 用户退出登录
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function LoginOut(params: object) {
	return request({
		url: '/platform/user/sign-out',
		method: 'post',
		data: params
	});
}

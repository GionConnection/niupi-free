
import request from '/@/utils/request';
export function getOne(params: object) {
    return request({
        url: '/platform/files/one',
        method: 'get',
        params,
    });
}
export function getList(params: object) {
    return request({
        url: '/platform/files/list',
        method: 'get',
        params,
    });
}
export function add(params: object) {
    return request({
        url: '/platform/files',
        method: 'post',
        data: params
    });
}
export function edit(params: object) {
    return request({
        url: '/subscribe/files/edit',
        method: 'post',
        data: params
    });
}
export function del(params: object) {
    return request({
        url: '/platform/files/del',
        method: 'post',
        data: params
    });
}
export function changeStatus(params: object) {
    return request({
        url: '/platform/files/change-status',
        method: 'post',
        data: params
    });
}
export function upload(params: object) {
    return request({
        url: '/platform/files/upload-file',
        method: 'post',
        data: params
    });
}
export function getImgFile(params: object) {
    return request({
        url: '/platform/files/img-file',
        method: 'get',
        params,
    });
}
﻿using Furion;
using Furion.FriendlyException;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace NiuPi.Tools
{
    /// <summary>
    /// 规范化RESTful风格返回值
    /// </summary>
    public class RestfulResult
    {
        private static RestfulResult? _intance;        public static RestfulResult Instance        {            get            {                if (_intance == null)                {                    _intance = new RestfulResult();                }                return _intance;            }        }

        /// <summary>
        /// 分页带数据的接口返回格式
        /// </summary>
        /// <param name="data">返回数据</param>
        /// <param name="extras">附加数据</param>
        /// <param name="message">结果描述</param>
        /// <returns></returns>
        public IActionResult OnPageSucceeded(object data, object extras = null, string message = "请求成功！")
        {
            return new JsonResult(new RestfulResult<object>
            {
                Code = data is null ? StatusCodes.Status204NoContent : StatusCodes.Status200OK,
                Success = true,
                Data = data,
                Message = message,
                Extras = extras,
                Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()
            });
        }
        /// <summary>
        /// 失败返回
        /// </summary>
        /// <param name="extras">附加数据</param>
        /// <param name="message">结果描述</param>
        /// <returns></returns>
        public IActionResult OnFailed(object extras = null, string message = "操作失败！")
        {
            throw Oops.Oh(JsonConvert.SerializeObject(new RestfulResultOh
            {
                Success = false,
                Message = message,
                Extras = extras,
                Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()
            })).StatusCode(201);
        }
        /// <summary>
        /// 成功返回
        /// </summary>
        /// <param name="extras">附加数据</param>
        /// <param name="message">结果描述</param>
        /// <returns></returns>
        public IActionResult OnSucceeded(object extras = null, string message = "操作成功！")
        {
            throw Oops.Oh(JsonConvert.SerializeObject(new RestfulResultOh
            {
                Success = true,
                Message = message,
                Extras = extras,
                Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()
            })).StatusCode(200);
        }
    }

    /// <summary>
    /// 分页带数据的接口返回格式模型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RestfulResult<T>
    {
        /// <summary>
        /// 执行成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 状态码
        /// </summary>
        public int? Code { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public object Message { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// 附加数据
        /// </summary>
        public object Extras { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public long Timestamp { get; set; }
    }
    /// <summary>
    /// 写操作带附加信息无数据的接口返回格式模型
    /// </summary>
    public class RestfulResultOh
    {
        /// <summary>
        /// 执行是否成功
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public object Message { get; set; }
        /// <summary>
        /// 附加数据
        /// </summary>
        public object Extras { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public long Timestamp { get; set; }
    }
}
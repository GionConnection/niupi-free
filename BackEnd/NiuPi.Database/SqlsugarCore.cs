﻿using Furion;
//依赖项注入容器
using Microsoft.Extensions.DependencyInjection;
using SqlSugar;

namespace NiuPi.Database
{
    public static class SqlsugarCore
    {
        public static void AddSqlsugarSetup(this IServiceCollection services)
        {
            SqlSugarScope sqlSugar = new SqlSugarScope(new List<ConnectionConfig>(){
                new ConnectionConfig() {
                    ConfigId = "0",
                    DbType = DbType.MySql,
                    ConnectionString = App.Configuration["Dbmaster"],
                    IsAutoCloseConnection = true,
                },
            }, db =>
            {
                db.GetConnection("0").Aop.OnLogExecuting = (sql, p) =>
                 {
                     //Console.WriteLine(sql);
                 };
            });
            services.AddSingleton(sqlSugar);
        }

    }
}
﻿using SqlSugar;
using NiuPi.Tools;

namespace NiuPi.Database.BasicModel
{
    /// <summary>
    /// 模型基类
    /// </summary>
    [TenantAttribute("0")]//多租户分库特性，暂时不用分库，只用一个库  在此系统作为门店ID
    public class Base
    {
        /// <summary>
        /// 编号
        /// </summary>
        [SugarColumn(ColumnDescription = "ID", IsIdentity = true, IsPrimaryKey = true)]
        public virtual long Id { get; set; } = 0;
        /// <summary>
        /// 多租户ID 在此系统作为门店ID
        /// </summary>
        [SugarColumn(ColumnDescription = "多租户ID", DefaultValue = "0", IsNullable = false)]
        public virtual long? TenantId { get; set; } = 0;
        /// <summary>
        /// 创建时间
        /// </summary>
        [SugarColumn(ColumnDescription = "创建时间", DefaultValue = "0", IsNullable = false)]
        public virtual long? CreatedTime { get; set; } = Time.GetTimeStamp();
        /// <summary>
        /// 更新时间
        /// </summary>
        [SugarColumn(ColumnDescription = "更新时间", DefaultValue = "0", IsNullable = false)]
        public virtual long? UpdatedTime { get; set; } = Time.GetTimeStamp();
        /// <summary>
        /// 创建者Id
        /// </summary>
        [SugarColumn(ColumnDescription = "创建者Id", DefaultValue = "0", IsNullable = false)]
        public virtual long? CreatedUserId { get; set; } = 0;
        /// <summary>
        /// 软删除
        /// </summary>
        [SugarColumn(ColumnDescription = "软删除标记", DefaultValue = "0", IsNullable = false)]
        public virtual int IsDeleted { get; set; } = 0;
        /// <summary>
        /// 基础状态
        /// </summary>
        [SugarColumn(ColumnDescription = "基础状态", DefaultValue = "0", IsNullable = false)]
        public virtual int Status { get; set; } = 0;
    }
}

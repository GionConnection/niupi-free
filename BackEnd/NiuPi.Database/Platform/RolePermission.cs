﻿using NiuPi.Database.BasicModel;
using SqlSugar;

namespace NiuPi.Database.Platform
{
    [SugarTable("role_permission")]
    public class RolePermission : Base
    {
        /// <summary>
        /// 角色关联ID
        /// </summary>
        [SugarColumn(ColumnDescription = "角色关联ID", DefaultValue = "0", IsNullable = false)]
        public int? RoleId { get; set; } = 0;
        /// <summary>
        /// 权限菜单名称
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "权限菜单名称", IsNullable = false)]
        public string? PermissionName { get; set; }
        /// <summary>
        /// 角色权限标识
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "角色权限标识", IsNullable = false)]
        public string? Permission { get; set; }
    }
}

# niupi-free

#### 介绍
furion+net6+sqlsugar+vue3+typescript+vue-next-admin细粒度权限控制的快速开发框架

#### 软件架构

- 后端：   furion+net6+sqlsugar
- 前端：   vue3+typescript+vue-next-admin
- 数据库： mysql
- 缓存：   redis


#### 安装教程

见Doc目录说明文件

#### 使用说明

见Doc目录说明文件


#### 获取项目

- git init 
- git remote add origin  https://gitee.com/GionConnection/niupi-free.git
- git pull origin master / git pull origin master --allow-unrelated-histories

#### 关于作者

- QQ：1844045442
- 微信公众号：量子互联网络科技（每日推荐一款好用的开发框架以及最新技术贴子）
- 另：接受广大用户的合理化功能建议免费开发维护，同时也接受个性化定制开发（各平台小程序/APP/游戏/网站/桌面程序/游戏/硬件/软件）
- 赞赏作者
- <img src="%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220215102243.jpg" width="200" height="200" />
- 关注技术型微信公众号
- <img src="%E6%89%AB%E7%A0%81_%E6%90%9C%E7%B4%A2%E8%81%94%E5%90%88%E4%BC%A0%E6%92%AD%E6%A0%B7%E5%BC%8F-%E6%A0%87%E5%87%86%E8%89%B2%E7%89%88.png"  />


#### 预览
![主页](https://images.gitee.com/uploads/images/2022/0413/120251_190743fc_962974.png "QQ图片20220413114513.png")
![菜单管理](https://images.gitee.com/uploads/images/2022/0413/120300_2a2cb236_962974.png "QQ图片20220413114645.png")
![角色管理](https://images.gitee.com/uploads/images/2022/0413/120309_5e70c7f3_962974.jpeg "QQ图片20220413114653.jpg")